July 6th, 2019:
- Added Base Protectors and Bots.
- Changed the Base Protectors.
- Added more grid types for config.json.
- Added a new type of rock.
- Added the launcher branch.
- Increased the map size.
- You spawn in the dominators now (Thx mse).
- Added invisibility (Thx mse).
- Added Stalker and Landmine.


July 7th, 2019:
- Added the Homing Branch.
- Decreased map size due to lag.
- Made bots a lower level.
- Invisible tanks now have a circle in the center of their body.
- Added Marine.
- Added Watcher.


July 8th, 2019:
- Removed Bots and downsized the map due to lag.
- Changed the map.
- Made booster faster and weaker.
- Redesigned Compass and it's evos.
- Added Follower.
- Made the map a rectangle.
- Added tier 0.
- Changed the gamespeed.


July 10th, 2019:
- Buffed Marine.


July 11th, 2019:
- Added bots back.
- Added Aimer.


July 12th, 2019:
- Renamed Aimer to Creeper.


July 13th, 2019:
- Added Smotherer and Berserker.
- Made the map a square but got rid of bots.


July 14th, 2019:
- Added a Basic Page 2.
- The server now restarts every 30 minutes (1800 seconds) to help the server running smoother. You can change the amount of time or remove this entirely (Thx IvyX).
- Optional warnings for when the server will restart have been added, which you can toggle in config.json (Thx IvyX).
- Added maze walls, so you can change the map so that the server is a maze mode (Thx IvyX).
- Added Subduer, Contagion, and Pathogen (Thx IvyX).
- Fixed CPU issues (THX A LOT IVYX)!
- Made Automatic server resets able to be toggled in config.json (Thx IvyX).
- Made it possible to change the length of the countdown before the server resets in config.json (Thx IvyX).
- Made it possible to toggle nest food increase for each server reset warning (Thx IvyX).


July 16th, 2019:
- Fixed an error with the template.
- Made turrets more precise.


August 9th, 2019 (First update in a while):
- Added tier scaling so you can choose the distance between tiers instead of what levels the tiers are (Thx IvyX).
- Added two new tiers.